# Pinewood Wikia Bot
This bot is used on [Pinewood Wikia](https://pinewood.fandom.com) to automate some tasks. It is open-sourced on Gitlab to allow collaboration and issue tracking.
# Running the bot
Sometimes, when making a pull request you might want to make sure it will work, to host the bot is simple, just follow these directions **this will be the only support for running the bot**:
This tutorial aussmes you already have the bot folder ready

 - Run `npm install` in the bot folder to install needed node modules
 - To begin setting up  the bot, run `npm run setup`, this will ask you for the mediawiki site, bot username, and bot password
 - Once setup is finished, you can start the bot with `npm start`
If the bot isn't working and you need more debug info, in the index.js script, under the client variable, add `debug: true`
**Disclaimer: This bot was designed to  run on FANDOM, while most of the features should still run on mediawiki as fandom is mediawiki in the core and API, you might need to change the path where the api.php script is located from '' (fandom stores it at the root directory) to "/w" (normal mediawiki stores it there), this  can be changed at the client variable in index.js. You may also need to change from https to http if your wiki does not support https.**