require('dotenv').config()

var bot = require('nodemw');
var https = require('https');

var client = new bot({
  protocol: 'https',
  server: process.env.MEDIAWIKISITE,
  username: process.env.BOTUSERNAME,
  password: process.env.BOTPASSWORD,
  path: ''
});

String.prototype.regexIndexOf = function(regex, startpos) {
  var indexOf = this.substring(startpos || 0).search(regex);
  return (indexOf >= 0) ? (indexOf + (startpos || 0)) : indexOf;
}

String.prototype.replaceAt=function(index, char) {
  var a = this.split("");
  a[index] = char;
  return a.join("");
}

String.prototype.insert = function(index, string) {
  if (index > 0)
  {
    return this.substring(0, index) + string + this.substring(index, this.length);
  }

  return string + this;
};

const formatMembers = n => {
  if (n < 1e3) return n;
  if (n >= 1e3 && n < 1e6) return +(n / 1e3).toFixed(1) + "K";
  if (n >= 1e6 && n < 1e9) return +(n / 1e6).toFixed(1) + "M";
  if (n >= 1e9 && n < 1e12) return +(n / 1e9).toFixed(1) + "B";
  if (n >= 1e12) return +(n / 1e12).toFixed(1) + "T";
};

function editpage( title, content, summary, minor, callback ) {
  let params = {
    text: content,
    bot: true,
    watchlist: 'unwatch'
  };

  if ( typeof minor === 'function' ) {
    callback = minor;
    minor = undefined;
  }

  if ( minor ) {
    params.minor = '';
  } else {
    params.notminor = '';
  }

  client.doEdit( 'edit', title, summary, params, callback );
}

client.logIn(function(err) {
  while (true) {
    client.getAllPages( function ( err, pages ) { // I am 100% aware transcluedin exists in the mediawiki API, however fandom is on an oudated version of mediawiki that doesn't have transcluedin, do not submit issues for this, it will be fixed once fandom updates (they are working on this currently so it won't be too long)
      pages.forEach(function (item, index) {
        client.getArticle(item.pageid, function(err, data) {
          var newdata = data
          var n = data.regexIndexOf(/{{GroupInfobox.*}}/);
          var e = data.regexIndexOf(/members\s{0,1}=/,n);
          if (e != -1) {
            var r = data.regexIndexOf(/(\||}})/,e+1);
            var t = data.indexOf("=",e);
            var i = r;
            do {
              newdata = newdata.replaceAt(i,"")
              i--
            }
            while (i > t);

            var y = data.regexIndexOf(/title1\s{0,1}=/,n);
            var u = data.regexIndexOf(/(\||}})/,y+1);
            var p = data.indexOf("=",y);

            var groupname = ''
            for (i=p+1; i<u; i++) { 
              groupname = groupname + data.charAt(i)
            }

            var urlgroupname = encodeURI(groupname)
            var robloxgrouplookupdata = '';
            var robloxgrouplookupdataobject = null;
            https.get(`https://groups.roblox.com/v1/groups/search/lookup?groupName=${urlgroupname}`, res => { // Search for group name
              res.on("data", d => {
                robloxgrouplookupdata += d
              })
              res.on("end", () => {
                robloxgrouplookupdataobject = JSON.parse(robloxgrouplookupdata)
                 if (typeof(robloxgrouplookupdataobject.errors) !== 'undefined') {
                  if (robloxgrouplookupdataobject.errors[0].message == 'TooManyRequests') { // If the request is rate limited, try again in 3 seconds, keep retring intil it sucessfully passes.
                  var done = false
                  require('deasync').loopWhile(function(){ //failed request loop
                    robloxgrouplookupdata = null
                    require('deasync').sleep(8000);
                    https.get(`https://groups.roblox.com/v1/groups/search/lookup?groupName=${urlgroupname}`, res => { // Search for group name
                      res.on("data", d => {
                        robloxgrouplookupdata += d
                      })
                      res.on("end", () => {
                        var done = true
                        robloxgrouplookupdataobject = JSON.parse(robloxgrouplookupdata)
                        if (typeof(robloxgrouplookupdataobject.errors) !== 'undefined') {
                          if (robloxgrouplookupdataobject.errors[0].message == 'TooManyRequests') { // If the request is rate limited, try again in 3 seconds, keep retring intil it sucessfully passes.
                              var done = false
                          }
                            var robloxgroupapidata = '';
                            var robloxgroupapidataobject = null;
                          
                          https.get(`https://groups.roblox.com/v1/groups/${robloxgrouplookupdataobject.data[0].id}`, (resp) => {  // Check for group stats

                          resp.on('data', (chunk) => {
                            robloxgroupapidata += chunk;
                          });
                          res.on("end", () => {
                            var done = true
                            robloxgrouplookupdataobject = null
                            robloxgrouplookupdataobject = JSON.parse(robloxgrouplookupdata)
                          })
                          })
                        }
                        })
                    })
                  return !done
                  })
                }
              }
                var robloxgroupapidata = '';
                var robloxgroupapidataobject = null;
              
              https.get(`https://groups.roblox.com/v1/groups/${robloxgrouplookupdataobject.data[0].id}`, (resp) => {  // Check for group stats

              resp.on('data', (chunk) => {
                robloxgroupapidata += chunk;
              });

              resp.on('end', () => {
                robloxgroupapidataobject = JSON.parse(robloxgroupapidata)
                if (typeof(robloxgroupapidataobject.errors) !== 'undefined') {
                  if (robloxgroupapidataobject.errors[0].message == 'TooManyRequests') { // If the request is rate limited, try again in 3 seconds, keep retring intil it sucessfully passes.
                    var done = false
                    robloxgroupapidataobject = null
                    require('deasync').loopWhile(function(){ //failed request loop
                      require('deasync').sleep(8000);
                        robloxgroupapidata = ""
                        https.get(`https://groups.roblox.com/v1/groups/${robloxgrouplookupdataobject.data[0].id}`, (resp) => { // Check for group stats
                        resp.on('data', (chunk) => {
                          robloxgroupapidata += chunk;
                        });
          
                        resp.on('end', () => {
                          robloxgroupapidataobject = ""
                          robloxgroupapidataobject = JSON.parse(robloxgroupapidata)
                          if (typeof(robloxgroupapidataobject.errors) !== 'undefined') {
                            if (robloxgroupapidataobject.errors[0].message == 'TooManyRequests') { // If the request is rate limited, try again in 3 seconds, keep retring intil it sucessfully passes.
                              var done = false
                            }
                          }
                        });
                        
          
                        }).on("error", (err) => {
                          console.log("Error: " + err.message);
                          
                          });
                        });
                  
                  }
                  return !done
                }
                
                robloxgroupapidataobject = JSON.parse(robloxgroupapidata)
                if (typeof(robloxgroupapidataobject.memberCount)!== "undefined") { // Don't update the page if the type is undefined for whatever reason
                    newdata = newdata.insert(t+1," "+ formatMembers(robloxgroupapidataobject.memberCount)+"|")
                    editpage(item.title,newdata,"Updated member count",true,function(err) {
                      if (err) throw err
                    })
                }
              });

              }).on("error", (err) => {
                console.log("Error: " + err.message);
                
              });
            
              })
            })
          }
        });
    });
  });
  require('deasync').sleep(3600000);
  }
})